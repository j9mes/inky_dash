#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime
import glob
import inky
import json
import requests
import socket
from Adafruit_IO import Client
from bs4 import BeautifulSoup
from collections import deque
from font_source_sans_pro import SourceSansProBold
from PIL import Image, ImageDraw, ImageFont
from threading import Thread
from time import sleep

inkyphat = inky.InkyPHAT('black')
screen_height_in_pixels = 104

key_list = ['summary',
            'high',
            'low',
            'wind_direction',
            'wind_value',
            'wind_unit',
            'weather_icon',
            'inside_temp',
            'pressure',
            'outside_humidity',
            'outside_temp',
            ]

inside_temp_list = deque([])
outside_temp_list = deque([])
inside_temp_plot_list = []
outside_temp_plot_list = []
outside_humidity_plot_list = []
outside_humidity_list = deque([])
pressure_plot_list = []
pressure_list = deque([])


def main():
    cat_feed_weight, feed_date = get_cat_feed_values()
    old_values = update_screen(cat_feed_weight, feed_date)

    update_graphs_thread.start()

    aio = Client('j9mes', 'aio_zNUk19NgrXFINPqnSkSRTrlmWQxu')

    while True:
        cat_feed_weight, feed_date = get_cat_feed_values()
        new_values = get_values()
        if new_values != old_values:
            old_values = update_screen(cat_feed_weight, feed_date)

        # update Adafruit IO dash:
        # inside_temp = new_values[7]
        # outside_temp = new_values[10]
        # aio.send_data('inside-temp', inside_temp)
        # aio.send_data('outside-temp', outside_temp)

        sleep(60)


def create_mask(source, mask=(inkyphat.WHITE, inkyphat.BLACK, inkyphat.RED)):
    """Create a transparency mask.

    Takes a paletized source image and converts it into a mask
    permitting all the colours supported by Inky pHAT (0, 1, 2)
    or an optional list of allowed colours.

    :param mask: Optional list of Inky pHAT colours to allow.

    """
    mask_image = Image.new("1", source.size)
    w, h = source.size
    for x in range(w):
        for y in range(h):
            p = source.getpixel((x, y))
            if p in mask:
                mask_image.putpixel((x, y), 255)

    return mask_image


# Dictionaries to store icons and icon masks in
icons = {}
masks = {}

# This maps the weather summary from Dark Sky
# to the appropriate weather icons
icon_map = {
    "snow": ["snow", "sleet"],
    "rain": ["rain"],
    "cloud": ["fog", "cloudy", "partly-cloudy-day", "partly-cloudy-night"],
    "sun": ["clear-day", "clear-night"],
    "storm": [],
    "wind": ["wind"]
}


def get_weather():

    wx = {}
    try:
        # Query Dark Sky (https://darksky.net/) to scrape current weather data
        # using hardcoded coordinates for Alverston St, Auckland

        res = requests.get("https://darksky.net/forecast/{}/ca12/en".format(",".join([str(c) for c in [-36.878127, 174.696823]])))
        
    except TypeError:
        pass
    try:
        if res.status_code == 200:
            soup = BeautifulSoup(res.content, "lxml")
            curr = soup.find_all("span", "currently")
            wx["summary"] = curr[0].img["alt"].split()[0]
            wx["temperature"] = int(curr[0].find("span", "summary").text.split()[0][:-1])
            press = soup.find_all("div", "pressure")
            wx["pressure"] = int(press[0].find("span", "num").text)
            _low = soup.find_all("span", "low-temp-text")
            wx["low"] = str(_low[0].text)
            _high = soup.find_all("span", "high-temp-text")
            wx["high"] = str(_high[0].text)
            _wind_direction = soup.find_all("div", "wind")
            wx["wind_direction"] = _wind_direction[0].contents[3].contents[5].attrs['title']
            _wind_value = soup.find_all("div", "wind")
            wx["wind_value"] = _wind_value[0].contents[3].contents[1].text
            _wind_unit = soup.find_all("div", "wind")
            wx["wind_unit"] = _wind_unit[0].contents[3].contents[3].text
            return wx
        else:
            return wx
    except UnboundLocalError:
        return wx


def get_values():

    with Bme280('192.168.86.25', 80) as outside_sensor, Bme280('192.168.86.23', 81) as inside_sensor:

        weather = get_weather()

        if weather:
            summary = weather["summary"]
            high = weather["high"]
            low = weather["low"]
            wind_direction = weather["wind_direction"]
            wind_value = weather["wind_value"]
            wind_unit = weather["wind_unit"]

            for icon in icon_map:
                if summary in icon_map[icon]:
                    wx_icon = icon
                    break

        else:
            summary = "x"
            high = 'x'
            low = 'x'
            wind_direction = 'x'
            wind_value = 'x'
            wind_unit = 'x'
            wx_icon = "x"

        return [summary,
                high,
                low,
                wind_direction,
                wind_value,
                wind_unit,
                wx_icon,
                inside_sensor.temperature,
                outside_sensor.pressure,
                outside_sensor.humidity,
                outside_sensor.temperature,
                ]


def get_cat_feed_values():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.settimeout(5)
        s.connect(('192.168.86.201', 2525))
        cat_feed = json.loads(s.recv(1024))
        weight = cat_feed["weight"]
        date = cat_feed["date"]
        return weight, date

    except (OSError, socket.timeout):
        date = datetime.datetime.now().timetuple().tm_yday
        return ' ! ', date


def update_screen_with_new_cat_feed():
    cat_feed_weight, feed_date = get_cat_feed_values()
    update_screen(cat_feed_weight, feed_date)


class Bme280:

    def __init__(self, address, port):
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.settimeout(60)
            s.connect((address, port))
            sensor = json.loads(s.recv(1024))

            self.temperature = str(round(sensor['temperature']))
            self.pressure = str(round(sensor['pressure']))

            # try to get humidity reading - if sensor is a Bmp280, no humidity reading will be available
            try:
                self.humidity = str(round(sensor['humidity']))
            except KeyError:
                pass

        except (OSError, socket.timeout):
            self.temperature = '0'
            self.pressure = '0'
            self.humidity = '0'

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        return self


def update_graphs():

    graph_pixel_width = 67
    right_pixel_limit = 125

    def plot_graph(new_reading, sensor_value_list,
                   lowest_val_displayed, lower_pixel_limit,
                   right_pixel_limit, graph_pixel_width, new_range, old_range):
        
        plot_list = []

        if len(sensor_value_list) > graph_pixel_width:
            sensor_value_list.popleft()
        # add inside_temp value to deque
        sensor_value_list.append(new_reading)

        for val in sensor_value_list:
            right_pixel_limit -= 1
            if right_pixel_limit < 0:
                break
            try:
                plot_list.append((right_pixel_limit, round(lower_pixel_limit - (((int(val) - lowest_val_displayed) * new_range) / old_range))))
            except ValueError:
                pass
        
        return plot_list

    def update_inside_temp_graph():
        global inside_temp_plot_list
        lowest_temp = 6
        lower_pixel_limit = 49  # pixel y value of lowest border of graph
        new_range = 50  # range of y axis of graph area in pixels
        old_range = 25  # range of temperature values (31 - 6)

        inside_temp_plot_list = plot_graph(inside_readings.temperature, inside_temp_list, lowest_temp,
                                           lower_pixel_limit, right_pixel_limit, graph_pixel_width, new_range, old_range)

    def update_outside_temp_graph():
        global outside_temp_plot_list
        lowest_temp = 6
        lower_pixel_limit = 49  # pixel y value of lowest border of graph
        new_range = 50  # range of y axis of graph area in pixels
        old_range = 25  # range of temperature values (31 - 6)

        outside_temp_plot_list = plot_graph(outside_readings.temperature, outside_temp_list, lowest_temp,
                                            lower_pixel_limit, right_pixel_limit, graph_pixel_width, new_range, old_range)

    def update_outside_humidity_graph():
        global outside_humidity_plot_list
        lowest_humidity = 15
        lower_pixel_limit = 106  # pixel y value of lowest border of graph
        new_range = 25  # range of y axis of graph area in pixels
        old_range = 85  # range of pressure values (100 - 15)

        outside_humidity_plot_list = plot_graph(outside_readings.humidity, outside_humidity_list, lowest_humidity,
                                                lower_pixel_limit, right_pixel_limit, graph_pixel_width, new_range, old_range)

    def update_pressure_graph():
        global pressure_plot_list
        lowest_pressure = 970
        lower_pixel_limit = 75  # pixel y value of lowest border of graph
        new_range = 26  # range of y axis of graph area in pixels
        old_range = 70  # range of pressure values (940hPa - 1040hPa)
        
        pressure_plot_list = plot_graph(outside_readings.pressure, pressure_list, lowest_pressure,
                                        lower_pixel_limit, right_pixel_limit, graph_pixel_width, new_range, old_range)

    while True:
        with Bme280('192.168.86.25', 80) as outside_readings, Bme280('192.168.86.23', 81) as inside_readings:
            update_inside_temp_graph()
            update_outside_temp_graph()
            update_outside_humidity_graph()
            update_pressure_graph()

            sleep(1289.55)


update_graphs_thread = Thread(target=update_graphs)


def update_screen(weight, date):
    global inside_temp_plot_list

    all_values = get_values()

    while all_values is None:
        print("all_values = " + str(all_values) + ". Retrying get_values(). " + str(datetime.datetime.now()))
        all_values = get_values()

    try:
        # create a dictionary of the data
        values = dict(zip(key_list, all_values))

    except TypeError as e:
        print('key_list:')
        print(key_list)
        print('all_values:')
        print(all_values)
        print('Error:')
        print(e)

    high = values.get('high')
    low = values.get('low')
    wind_direction = values.get('wind_direction')
    wind_value = values.get('wind_value')
    wind_unit = values.get('wind_unit')
    weather_icon = values.get('weather_icon')
    inside_temp = values.get('inside_temp')
    pressure = values.get('pressure')
    humidity = values.get('outside_humidity')
    outside_temp = values.get('outside_temp')

    # Load the fonts
    smallest_font = ImageFont.truetype(SourceSansProBold, 14)
    font = ImageFont.truetype(SourceSansProBold, 18)

    img = Image.open('resources/dash_bg.png')
    draw = ImageDraw.Draw(img)

    # Load icon files and generate masks
    for icon in glob.glob("resources/icon-*.png"):
        icon_name = icon.split("icon-")[1].replace(".png", "")
        icon_image = Image.open(icon)
        icons[icon_name] = icon_image
        masks[icon_name] = create_mask(icon_image)

    try:

        # inside temp
        draw.text((16, 0), inside_temp, inkyphat.WHITE, font=font)

        # outside temp
        draw.text((16, 26), outside_temp, inkyphat.BLACK, font=font)

        # high
        draw.text((174, 25), "H: " + high, inkyphat.WHITE, font=smallest_font)

        # low
        draw.text((174, 42), "L: " + low, inkyphat.WHITE, font=smallest_font)

        # wind
        draw.text((135, 60), wind_direction + ' ' + wind_value + ' ' + wind_unit, inkyphat.WHITE, font=smallest_font)

        # cat feed
        day_of_year = datetime.datetime.now().timetuple().tm_yday

        if day_of_year == date:
            draw.text((169, 80), str(weight), inkyphat.BLACK, font=font)
        else:
            draw.text((169, 80), '...', inkyphat.BLACK, font=font)

        # pressure
        draw.text((16, 52), pressure, inkyphat.WHITE, font=font)

        # humidity
        draw.text((17, 77), humidity, inkyphat.BLACK, font=font)

        # date
        now = datetime.datetime.now()
        draw.text((134, 3), now.strftime("%a %d %b"), inkyphat.BLACK, font=smallest_font)

        # Draw the current weather icon over the backdrop
        if weather_icon != 'x':
            img.paste(icons[weather_icon], (130, 20), masks[weather_icon])
        else:
            pass

        # inside_temp graph
        draw.point(inside_temp_plot_list, inkyphat.BLACK)

        # outside_temp graph
        draw.point(outside_temp_plot_list, inkyphat.BLACK)

        # pressure graph
        draw.point(pressure_plot_list, inkyphat.WHITE)

        # outside_humidity graph
        draw.point(outside_humidity_plot_list, inkyphat.BLACK)

    except TypeError as e:
        print(e)
        print('##### all_values:')
        print(all_values)
        print('#######')

    inkyphat.set_image(img)

    inkyphat.show()

    return all_values


main()
